package hibatis.annotation;

import java.lang.annotation.*;

/**
 * Created by dave on 18-6-15 下午9:20.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
public @interface Generated {
    boolean value() default true;
}
