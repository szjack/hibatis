package hibatis.annotation;

import hibatis.ConflictAction;
import hibatis.FilterType;

import java.lang.annotation.*;

/**
 * Created by huangdachao on 2018/6/13 16:17.
 * 查询类中过滤字段标注
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
public @interface Filter {
    int DEFAULT_ORDER = 9999;

    /**
     * 当过滤参数是简单数据类型时，可以指定过滤参数对应的实体类字段名
     * @return
     */
    String field() default "";

    /**
     * 在过滤条件中的排序，值小的在过滤条件中靠前位置。
     */
    int order() default DEFAULT_ORDER;

    /**
     * 对应的表名，为空时使用主表。表名可以是查询类的@Query标记中定义的表，也可以是实体类@Entity标记中定义的表
     * @return
     */
    String table() default "";

    /**
     * 表别名，用于多态表自Join时
     * @return
     */
    String tblAlias() default "";

    /**
     * 表列名，为空时，使用实体类同名字段的列名，同时表名与实体类同名字段一致
     * @return
     */
    String column() default "";

    /**
     * 过滤使用的运算符
     * @return
     */
    FilterType type() default FilterType.eq;

    /**
     * 过滤条件值的占位符，比如 "year(#{$@})"，这里的'$@'将被过滤字段名替换
     * @return
     */
    String placeholder() default "";

    /**
     * 是否忽略本条件
     * @return
     */
    boolean ignore() default false;

    /**
     * 对于Number或int, long, float, double类型字段，是否在字段值为0时忽略本过滤条件
     * @return
     */
    boolean ignoreOnZero() default false;

    /**
     * 当相同的TableColumn过滤条件存在时的行为
     * @return
     */
    ConflictAction conflict() default ConflictAction.coexist;
}
