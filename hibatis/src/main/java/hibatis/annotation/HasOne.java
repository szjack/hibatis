package hibatis.annotation;

import java.lang.annotation.*;

/**
 * Created by huangdachao on 2018/6/13 18:47.
 */
@Target(ElementType.FIELD)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface HasOne {
    /**
     * 查询条件
     * @return
     */
    Filter[] filter() default {};

    /**
     * 关联的多条记录排序。当指定orderBy时，生成查询语句自动添加limit 1
     * @return
     */
    String orderBy() default "";

    /**
     * 是否强制限定查询结果为1条，即生成查询语句自动添加limit 1
     * @return
     */
    boolean forceOne() default false;

    /**
     * 支持的视图。查询时如果指定视图，返回结果中的字段会根据view属性进行过滤。
     * 如果指定的视图在view属性中则映射该字段，否则不映射字段。
     * 如果字段没有指定view属性或属性为空数组，则总是映射该字段。
     * 如果查询时没有指定视图，则只映射没有指定视图的字段。
     * @return
     */
    String[] view() default {};

    /**
     * 映射结果视图
     * @return
     */
    String[] resultView() default {};
}
