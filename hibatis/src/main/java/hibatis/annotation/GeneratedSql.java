package hibatis.annotation;

import java.lang.annotation.*;

/**
 * Created by huangdachao on 2018/6/14 17:02.
 */
@Target(ElementType.ANNOTATION_TYPE)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface GeneratedSql {
    enum Type {
        /** 插入 */
        INSERT,

        /** 更新 */
        UPDATE,

        /** 删除 */
        DELETE,

        /** 查询 */
        SELECT,

        /** 查询条数 */
        COUNT
    }

    /**
     * 生成语句类型
     * @return
     */
    Type type();

    /**
     * 拦截器，必须是当前Mapper接口的default方法或静态方法
     * 方法签名：HibatisInterceptor.interceptor
     * @return
     */
    String interceptor() default "";
}
