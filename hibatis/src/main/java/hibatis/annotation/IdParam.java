package hibatis.annotation;

import hibatis.ConflictAction;

import java.lang.annotation.*;

/**
 * Created by huangdachao on 2018/6/15 14:47.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface IdParam {
    /**
     * 对应的主键字段名。当实体类有多个主键字段，且主键使用多个参数传递时需要指定
     * @return
     */
    String value() default "";

    /**
     * 当相同的TableColumn过滤条件存在时的行为
     * @return
     */
    ConflictAction conflictAction() default ConflictAction.coexist;
}
