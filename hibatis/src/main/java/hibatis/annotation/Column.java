package hibatis.annotation;

import java.lang.annotation.*;

/**
 * Created by huangdachao on 2018/6/13 16:17.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Column {
    String column() default ""; // 对应的数据库列名

    String table() default ""; // 表名，为空时使用主表名。关联表字段时必须指定关联表名。

    /**
     * 表别名，用于多态表自Join时
     * @return
     */
    String tblAlias() default "";

    /**
     * 是否忽略此字段。为true时，mybatis不再处理本字段
     * @return
     */
    boolean ignore() default false;

    /**
     * 是否可插入
     * @return
     */
    boolean insertable() default true;

    /**
     * 是否可更新
     * @return
     */
    boolean updateable() default true;

    /**
     * 支持的视图。查询时如果指定视图，返回结果中的字段会根据view属性进行过滤。
     * 如果指定的视图在view属性中则映射该字段，否则不映射字段。
     * 如果字段没有指定view属性或属性为空数组，则总是映射该字段。
     * 如果查询时没有指定视图，则只映射没有指定视图的字段。
     * @return
     */
    String[] view() default {};
}
