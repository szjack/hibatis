package hibatis;

/**
 * Created by huangdachao on 2018/7/3 15:16.
 */
public enum ConflictAction {
    /**
     * 覆盖
     */
    override,

    /**
     * 忽略
     */
    discard,

    /**
     * 同时保留
     */
    coexist,
}
